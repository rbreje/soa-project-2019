package ro.breje.exapp.fitpartner.model;

import java.util.List;

public class ActivitySearch {

	private int durationFrom;
	private int durationTo;
	private List<String> description;

	private EActivitySearchTypes searchType;

	public int getDurationFrom() {
		return durationFrom;
	}

	public void setDurationFrom(int durationFrom) {
		this.durationFrom = durationFrom;
	}

	public int getDurationTo() {
		return durationTo;
	}

	public void setDurationTo(int durationTo) {
		this.durationTo = durationTo;
	}

	public List<String> getDescription() {
		return description;
	}

	public void setDescription(List<String> description) {
		this.description = description;
	}

	public EActivitySearchTypes getSearchType() {
		return searchType;
	}

	public void setSearchType(EActivitySearchTypes searchType) {
		this.searchType = searchType;
	}

}
