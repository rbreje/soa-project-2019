package ro.breje.exapp.fitpartner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ro.breje.exapp.fitpartner.client.ActivityClient;
import ro.breje.exapp.fitpartner.client.ActivitySearchClient;

@Configuration
public class AppConfig {

	@Bean
	public ActivityClient client() {
		return new ActivityClient();
	}

	@Bean
	public ActivitySearchClient searchClient() {
		return new ActivitySearchClient();
	}

}
