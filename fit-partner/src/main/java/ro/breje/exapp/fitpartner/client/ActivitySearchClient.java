package ro.breje.exapp.fitpartner.client;

import java.net.URI;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import ro.breje.exapp.fitpartner.exceptions.ExAppException;
import ro.breje.exapp.fitpartner.model.Activity;
import ro.breje.exapp.fitpartner.model.ActivitySearch;

public class ActivitySearchClient {

	private Client client;

	public ActivitySearchClient() {
		client = ClientBuilder.newClient();
	}

	public List<Activity> search(ActivitySearch search) throws ExAppException {
		URI uri = UriBuilder.fromUri("http://localhost:8081/exercise-services/api").path("search/activities").build();
		WebTarget target = client.target(uri);
		Response response = target.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(search, MediaType.APPLICATION_JSON));
		if (response.getStatus() != 200) {
			throw new ExAppException(response.getStatus() + " : there was an error on the server.");
		}
		return response.readEntity(new GenericType<List<Activity>>() {
		});
	}

}
