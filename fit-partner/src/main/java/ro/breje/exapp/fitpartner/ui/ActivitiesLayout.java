package ro.breje.exapp.fitpartner.ui;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import ro.breje.exapp.fitpartner.client.ActivityClient;
import ro.breje.exapp.fitpartner.client.ActivitySearchClient;
import ro.breje.exapp.fitpartner.exceptions.ExAppException;
import ro.breje.exapp.fitpartner.model.Activity;
import ro.breje.exapp.fitpartner.model.ActivitySearch;

@SpringComponent
public class ActivitiesLayout extends VerticalLayout {

	private static final long serialVersionUID = 8573811380384689319L;

	@Autowired
	private ActivityClient client;

	@Autowired
	private ActivitySearchClient searchClient;

	@PostConstruct
	public void init() {
		update();
	}

	public void update() {
		setActivities(client.get());
	}

	public void update(String searchCriteria) {
		ActivitySearch activitySearch = new ActivitySearch();
		activitySearch.setDescription(Arrays.asList(searchCriteria));
		try {
			setActivities(searchClient.search(activitySearch));
		} catch (ExAppException e) {
			Notification.show(e.toString());
		}
	}

	private void setActivities(List<Activity> activities) {
		removeAllComponents();
		activities.forEach(activity -> addComponent(new ActivityItemLayout(activity)));

	}

	public void add(Activity activity) {
		try {
			client.create(activity);
		} catch (ExAppException e) {
			Notification.show(e.toString());
		}
		update();
	}

}
