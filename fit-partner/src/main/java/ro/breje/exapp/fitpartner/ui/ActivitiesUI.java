package ro.breje.exapp.fitpartner.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import ro.breje.exapp.fitpartner.model.Activity;

@SpringUI
public class ActivitiesUI extends UI {

	private static final long serialVersionUID = 3557660074814665687L;

	private VerticalLayout root;

	@Autowired
	private ActivitiesLayout activitiesLayout;

	@Override
	protected void init(VaadinRequest request) {
		setupLayout();
		addHeader();
		addForm();
		addActivities();
	}

	private void setupLayout() {
		root = new VerticalLayout();
		root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		setContent(root);
	}

	private void addHeader() {
		HorizontalLayout headerLayout = new HorizontalLayout();

		Label header = new Label("Activities");
		header.addStyleName(ValoTheme.LABEL_H1);
		TextField search = new TextField();
		Button searchButton = new Button();
		searchButton.setIcon(VaadinIcons.SEARCH);
		searchButton.addClickListener(click -> {
			activitiesLayout.update(search.getValue().trim());
			search.clear();
		});

		headerLayout.addComponentsAndExpand(header);
		headerLayout.addComponents(search, searchButton);

		root.addComponent(headerLayout);
	}

	private void addForm() {
		HorizontalLayout formLayout = new HorizontalLayout();
		formLayout.setWidth("80%");
		TextField description = new TextField();
		TextField duration = new TextField();

		Button addButton = new Button();
		addButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
		addButton.setIcon(VaadinIcons.PLUS);

		addButton.addClickListener(click -> {
			String descriptionValue = description.getValue().trim();
			String durationValue = duration.getValue().trim();
			if (!descriptionValue.isEmpty() && !durationValue.isEmpty()) {
				activitiesLayout.add(new Activity(descriptionValue, durationValue));
				duration.clear();
				description.clear();
			}
			description.focus();
		});
		description.focus();
		addButton.setClickShortcut(KeyCode.ENTER);
		formLayout.addComponentsAndExpand(description);
		formLayout.addComponents(duration, addButton);
		root.addComponent(formLayout);
	}

	private void addActivities() {
		activitiesLayout.setWidth("80%");
		root.addComponent(activitiesLayout);
	}

}
