package ro.breje.exapp.fitpartner.exceptions;

public class ExAppException extends Exception {

	private static final long serialVersionUID = -8600341012532908384L;

	public ExAppException() {
		super();
	}

	public ExAppException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ExAppException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ExAppException(String arg0) {
		super(arg0);
	}

	public ExAppException(Throwable arg0) {
		super(arg0);
	}

}
