package ro.breje.exapp.fitpartner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitPartnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitPartnerApplication.class, args);
	}

}

