package ro.breje.exapp.fitpartner.ui;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import ro.breje.exapp.fitpartner.client.ActivityClient;
import ro.breje.exapp.fitpartner.exceptions.ExAppException;
import ro.breje.exapp.fitpartner.model.Activity;

public class ActivityItemLayout extends HorizontalLayout {

	private static final long serialVersionUID = 7142862639036967272L;

	private final Long id;

	private final TextField description;

	private final TextField duration;

	private final Button edit;

	private final Button delete;

	private ActivityClient client = new ActivityClient();

	public ActivityItemLayout(Activity activity) {
		id = activity.getId();
		description = new TextField();
		description.setStyleName(ValoTheme.TEXTFIELD_BORDERLESS);
		duration = new TextField();
		duration.setStyleName(ValoTheme.TEXTFIELD_BORDERLESS);
		edit = new Button();
		edit.setIcon(VaadinIcons.EDIT);

		delete = new Button();
		delete.setIcon(VaadinIcons.ERASER);

		edit.addClickListener(click -> {
			Activity newActivity = new Activity(description.getValue(), duration.getValue());
			newActivity.setId(id);
			try {
				client.update(newActivity);
			} catch (ExAppException e) {
				Notification.show(e.toString());
			}
		});
		delete.addClickListener(click -> {
			try {
				client.delete(String.valueOf(id));
			} catch (ExAppException e) {
				Notification.show(e.toString());
			}
			update();
		});

		addComponentsAndExpand(description);
		addComponents(duration, edit, delete);

		setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

		Binder<Activity> binder = new Binder<>(Activity.class);
		binder.bindInstanceFields(this);
		binder.setBean(activity);
	}

	private void update() {
		removeAllComponents();

	}

}
