package ro.breje.exapp.fitpartner.model;

/**
 * 
 * @author Raul Breje
 *
 */
public class Activity {

	private long id;

	private String description;

	private String duration;

	public Activity() {
		super();
	}

	public Activity(String description, String duration) {
		super();
		this.description = description;
		this.duration = duration;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
