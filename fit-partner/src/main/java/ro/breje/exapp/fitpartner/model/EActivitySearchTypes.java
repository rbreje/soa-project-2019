package ro.breje.exapp.fitpartner.model;

public enum EActivitySearchTypes {

	SEARCH_BY_DURATION_RANGE, SEARCH_BY_DESCRIPTION;

}
