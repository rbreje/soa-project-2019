package ro.breje.exapp.client;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ro.breje.exapp.model.Activity;
import ro.breje.exapp.model.ActivitySearch;
import ro.breje.exapp.model.EActivitySearchTypes;

public class ActivityClientTest {

//	@Test
	public void testGet() {
		ActivityClient client = new ActivityClient();

		Activity activity = client.get("1254");
		System.out.println(activity);
		assertNotNull(activity);
	}

//	@Test
	public void testGetList() {
		ActivityClient client = new ActivityClient();

		List<Activity> activity = client.get();
		System.out.println(activity);
		assertNotNull(activity);
	}

//	@Test(expected = RuntimeException.class)
	public void testGetWithBadRequest() {
		ActivityClient client = new ActivityClient();
		client.get("");
	}

//	@Test(expected = RuntimeException.class)
	public void testGetWithNullActivityIdRequest() {
		ActivityClient client = new ActivityClient();
		client.get("7777");
	}

//	@Test
	public void testCreate() {
		ActivityClient client = new ActivityClient();

		Activity activity = new Activity();
		activity.setDescription("jogging");
		activity.setDuration(12);

		activity = client.create(activity);
		assertNotNull(activity);

	}

//	@Test
	public void testPut() {
		ActivityClient client = new ActivityClient();

		Activity activity = new Activity();
		// activity.setId(Long.parseLong("3456"));
		activity.setDescription("jogging");
		activity.setDuration(12);

		activity = client.update(activity);
		assertNotNull(activity);

	}

//	@Test
	public void testDelete() {
		ActivityClient client = new ActivityClient();
		client.delete("3333");
	}

//	@Test
	public void testSearch() {
		ActivitySearchClient client = new ActivitySearchClient();
		String param = "description";
		List<String> searchValues = Arrays.asList("swimming", "running");

		String secondParam = "durationFrom";
		int durationFrom = 30;
		String thirdParam = "durationTo";
		int durationTo = 55;

		// List<Activity> activities = client.search(param, searchValues);
		List<Activity> activities = client.search(param, searchValues, secondParam, durationFrom, thirdParam,
				durationTo);
		System.out.println(activities);
		assertNotNull(activities);
	}

//	@Test
	public void testSearchObject() {
		ActivitySearchClient client = new ActivitySearchClient();
		List<String> searchValues = Arrays.asList("biking", "running");
		ActivitySearch search = new ActivitySearch();
		search.setDescription(searchValues);
		search.setDurationFrom(30);
		search.setDurationTo(45);
		search.setSearchType(EActivitySearchTypes.SEARCH_BY_DESCRIPTION);
		List<Activity> activities = client.search(search);
		System.out.println(activities);
		assertNotNull(activities);

	}

}
