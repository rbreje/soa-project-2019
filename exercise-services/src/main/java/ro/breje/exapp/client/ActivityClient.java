package ro.breje.exapp.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ro.breje.exapp.exceptions.ExerciseAppException;
import ro.breje.exapp.model.Activity;

public class ActivityClient {

	private Client client;

	public ActivityClient() {
		client = ClientBuilder.newClient();
	}

	public Activity get(String id) {
		WebTarget target = client.target("http://localhost:8081/exercise-services/api/");
		// Activity response = target.path("activities/" +
		// id).request().get(Activity.class);
		Response response = target.path("activities/" + id).request(MediaType.APPLICATION_JSON).get(Response.class);
		if (response.getStatus() != 200) {
			throw new RuntimeException(response.getStatus() + " : there was an error on the server.");
			// TODO can use checked exception of your own
		}
		return response.readEntity(Activity.class);
	}

	public List<Activity> get() {
		WebTarget target = client.target("http://localhost:8081/exercise-services/api/");
		List<Activity> response = target.path("activities").request(MediaType.APPLICATION_JSON).get(List.class);
		// List<Activity> response =
		// target.path("activities").request(MediaType.APPLICATION_JSON)
		// .get(new GenericType<List<Activity>>() {
		// });
		return response;
	}

	public Activity create(Activity activity) throws ExerciseAppException {
		WebTarget target = client.target("http://localhost:8081/exercise-services/api/");
		Response response = target.path("activities/activity").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(activity, MediaType.APPLICATION_JSON));
		if (response.getStatus() != 200) {
			throw new ExerciseAppException(response.getStatus() + " : there was an error on the server.");
		}
		return response.readEntity(Activity.class);
	}

	public Activity update(Activity activity) throws ExerciseAppException {
		WebTarget target = client.target("http://localhost:8081/exercise-services/api/");
		Response response = target.path("activities/" + "activity.getId()").request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(activity, MediaType.APPLICATION_JSON));
		if (response.getStatus() != 200) {
			throw new ExerciseAppException(response.getStatus() + " : there was an error on the server.");
		}
		return response.readEntity(Activity.class);
	}

	public void delete(String activityId) throws ExerciseAppException {
		WebTarget target = client.target("http://localhost:8081/exercise-services/api/");
		Response response = target.path("activities/" + activityId).request(MediaType.APPLICATION_JSON).delete();
		if (response.getStatus() != 200) {
			throw new ExerciseAppException(response.getStatus() + " : there was an error on the server.");
		}
	}

}
