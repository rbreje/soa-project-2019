package ro.breje.exapp.exceptions;

public class ExerciseAppException extends Exception {

	private static final long serialVersionUID = -8600341012532908384L;

	public ExerciseAppException() {
		super();
	}

	public ExerciseAppException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ExerciseAppException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ExerciseAppException(String arg0) {
		super(arg0);
	}

	public ExerciseAppException(Throwable arg0) {
		super(arg0);
	}

}
