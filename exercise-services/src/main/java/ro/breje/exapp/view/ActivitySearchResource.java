package ro.breje.exapp.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.breje.exapp.dto.ActivityDto;
import ro.breje.exapp.model.Activity;
import ro.breje.exapp.model.ActivitySearch;
import ro.breje.exapp.service.ActivityService;

@Path("search/activities")
@Component
public class ActivitySearchResource {

	@Autowired
	private ActivityService activityService;

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response searchForActivities(ActivitySearch search) {
		System.out.println(search.getDescription() + " " + search.getDurationFrom() + " " + search.getDurationTo());
		List<ActivityDto> activities = activityService.findByConstraints(search);
		if (Objects.isNull(activities) || activities.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		List<Activity> result = new ArrayList<>();
		for (ActivityDto activityDto : activities) {
			Activity activity = new Activity();
			BeanUtils.copyProperties(activityDto, activity);
			result.add(activity);
		}
		return Response.ok().entity(new GenericEntity<List<Activity>>(result) {
		}).build();
	}
}
