package ro.breje.exapp.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.breje.exapp.dto.ActivityDto;
import ro.breje.exapp.model.Activity;
import ro.breje.exapp.service.ActivityService;

@Component
@Path("activities")
public class ActivityResource {

	@Autowired
	private ActivityService activityService;

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Activity> getAllActivities() {
		List<Activity> result = new ArrayList<>();
		List<ActivityDto> foundUsers = activityService.getAllActivities();
		for (ActivityDto activityDto : foundUsers) {
			Activity activity = new Activity();
			BeanUtils.copyProperties(activityDto, activity);
			result.add(activity);
		}
		return result;
	}

	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Path("{activityId}")
	public Response getActivity(@PathParam("activityId") String activityId) {
		if (StringUtils.isBlank(activityId)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		ActivityDto activityDto = activityService.getActivity(activityId);
		if (Objects.isNull(activityDto)) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Activity activity = new Activity();
		BeanUtils.copyProperties(activityDto, activity);
		return Response.ok().entity(activity).build();
	}

	@POST
	@Path("activity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Activity createActivity(Activity activity) {
		System.out.println(activity.getDescription());
		System.out.println(activity.getDuration());
		ActivityDto activityDto = new ActivityDto();
		BeanUtils.copyProperties(activity, activityDto);
		activityService.save(activityDto);
		return activity;
	}

	@PUT
	@Path("{activityId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response updateActivity(Activity activity) {
		ActivityDto activityDto = new ActivityDto();
		BeanUtils.copyProperties(activity, activityDto);
		activityService.update(activityDto);
		return Response.ok().entity(activity).build();
	}

	@DELETE
	@Path("{activityId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response deleteActivity(@PathParam("activityId") String activityId) {
		System.out.println(activityId);
		if (Objects.nonNull(activityId)) {
			activityService.delete(activityId);
		}
		return Response.ok().build();
	}

}
