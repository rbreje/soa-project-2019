package ro.breje.exapp.view;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.breje.exapp.service.AuthenticationService;

@Component
@Path("auth")
@Deprecated
public class UserResource {

	@SuppressWarnings("unused")
////	@Autowired
	private AuthenticationService service;

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response authenticate(MultivaluedMap<String, String> formParam) {
		System.out.println(formParam.getFirst("username").trim());
		System.out.println(formParam.getFirst("password").trim());
		return Response.status(Status.NOT_IMPLEMENTED).build();
	}

}
