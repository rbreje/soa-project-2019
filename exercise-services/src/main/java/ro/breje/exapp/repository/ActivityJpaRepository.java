package ro.breje.exapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.breje.exapp.entities.ActivityEntity;

public interface ActivityJpaRepository extends JpaRepository<ActivityEntity, Long> {

}
