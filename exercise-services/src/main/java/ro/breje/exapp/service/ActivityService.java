package ro.breje.exapp.service;

import java.util.List;

import ro.breje.exapp.dto.ActivityDto;
import ro.breje.exapp.model.ActivitySearch;

public interface ActivityService {

	List<ActivityDto> getAllActivities();

	void save(ActivityDto activity);

	void update(ActivityDto activity);

	void delete(String id);

	ActivityDto getActivity(String activityId);

	List<ActivityDto> findByConstraints(ActivitySearch search);

}
