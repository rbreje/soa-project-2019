package ro.breje.exapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.breje.exapp.dto.ActivityDto;
import ro.breje.exapp.entities.ActivityEntity;
import ro.breje.exapp.model.ActivitySearch;
import ro.breje.exapp.repository.ActivityJpaRepository;
import ro.breje.exapp.service.ActivityService;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityJpaRepository activityRepository;

	@Override
	public List<ActivityDto> getAllActivities() {
		List<ActivityDto> result = new ArrayList<>();
		List<ActivityEntity> foundRecords = Lists.newArrayList(activityRepository.findAll());
		for (ActivityEntity activityEntity : foundRecords) {
			ActivityDto activityDto = new ActivityDto();
			BeanUtils.copyProperties(activityEntity, activityDto);
			result.add(activityDto);
		}
		return result;
	}

	@Override
	public void save(ActivityDto activity) {
		ActivityEntity activityEntity = new ActivityEntity();
		BeanUtils.copyProperties(activity, activityEntity);
		activityRepository.save(activityEntity);
		// TODO make some error handling here
	}

	@Override
	public void update(ActivityDto activityDto) {
		if (Objects.nonNull(activityRepository.findOne(activityDto.getId()))) {
			ActivityEntity activityEntity = new ActivityEntity();
			BeanUtils.copyProperties(activityDto, activityEntity);
			activityRepository.save(activityEntity);
		}
	}

	@Override
	public void delete(String id) {
		activityRepository.delete(Long.parseLong(id));
	}

	@Override
	public ActivityDto getActivity(String activityId) {
		ActivityEntity activityEntity = activityRepository.findOne(Long.parseLong(activityId));
		ActivityDto activityDto = new ActivityDto();
		BeanUtils.copyProperties(activityEntity, activityDto);
		return activityDto;
	}

	@Override
	public List<ActivityDto> findByConstraints(ActivitySearch search) {
		List<ActivityDto> result = new ArrayList<>();
		List<ActivityDto> foundActivities = getAllActivities();
		for (ActivityDto activityDto : foundActivities) {
			if (activityDto.getDescription().contains(search.getDescription().get(0))) {
				result.add(activityDto);
			}
		}
		return result;
	}

}
