package ro.breje.exapp.model;

public enum EActivitySearchTypes {

	SEARCH_BY_DURATION_RANGE, SEARCH_BY_DESCRIPTION;

}
