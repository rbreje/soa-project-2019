package ro.breje.exapp.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Raul Breje
 *
 */
@XmlRootElement
public class Activity {

	private long id;

	private String description;

	private int duration;

	public Activity() {
		super();
	}

	public Activity(String description, int duration) {
		super();
		this.description = description;
		this.duration = duration;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
